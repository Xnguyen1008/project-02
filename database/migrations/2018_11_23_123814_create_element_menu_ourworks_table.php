<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementMenuOurworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_menu_ourworks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('name_image')->nullable();
            $table->string('link_image')->nullable();
            $table->string('link_event')->nullable();
            $table->unsignedInteger('menu_id');
            $table->foreign('menu_id')->references('id')->on('our_works_menu')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_menu_ourworks');
    }
}
