$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-services",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "services";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var title = $("#title" + id).html();
        var subtitle = $("#subtitle" + id).html();
        var content_subtitle = $("#content_subtitle" + id).html();
        $('#update_modal').val(id);
        $('#title_modal').val(title);
        $('#subtitle_modal').val(subtitle);
        $('#content_subtitle_modal').val(content_subtitle);

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var title = $('#title_modal').val();
        var subtitle = $('#subtitle_modal').val();
        var image = $('#image_modal').val();
        var content_subtitle = $('#content_subtitle_modal').val();
        var data = {
            'id': id,
            'title': title, 
            'subtitle': subtitle,
            'image': image,
            'content_subtitle': content_subtitle,
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-services",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#title' + id).html(data.title);
                $('#subtitle' + id).html(data.subtitle);
                $('#image' + id).val(data.image);
                $('#content_subtitle' + id).html(data.content_subtitle);
                $('#modal-info').modal('hide');//update va2 close modal
                
            }
        })
    });
})
