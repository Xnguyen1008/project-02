$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-menu",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "menu";

            }
        })
    })
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var menu_item = $("#menu_item" + id).html();
        var link_address = $("#link_address" + id).html();
        var logo = $("#logo" + id).html();

        $('#update_modal').val(id);
        $('#menu_item_modal').val(menu_item);
        $('#link_address_modal').val(link_address);
        $('#logo_modal').val(logo);

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var menu_item = $('#menu_item_modal').val();
        var link_address = $('#link_address_modal').val();
        var logo = $('#logo_modal').val();
        var data = {
            'id': id,
            'menu_item': menu_item,
            'link_address': link_address,
            'logo': logo
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-menu",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#menu_item' + id).html(data.menu_item);
                $('#link_address' + id).html(data.link_address);
                $('#logo' + id).html(data.logo);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
