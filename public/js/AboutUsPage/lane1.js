$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-lane1",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "about-us-lane1";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var title = $("#title" + id).html();
        var description = $("#description" + id).html();
        var alt_img = $("#alt" + id).html();
        var lane = $("#lane" + id).html();
        $('#update_modal').val(id);
        $('#title_modal').val(title);
        $('#description_modal').val(description);
        $('#alt_img_modal').val(alt_img);
        $('#lane_modal').val(lane);

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var title = $('#title_modal').val();
        var description = $('#description_modal').val();
        var image = $('#image_modal').val();
        var logo = $('#logo_modal').val();
        var alt_img = $('#alt_img_modal').val();
        var lane = $('#lane_modal').val();
        var data = {
            'id': id,
            'title': title, 
            'description': description,
            'image': image,
            'logo': logo,
            'alt_img': alt_img,
            'lane':lane
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-lane1",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#title' + id).html(data.title);
                $('#description' + id).html(data.descrtiption);
                $('#image' + id).val(data.image);
                $('#alt' + id).html(data.alt);
                $('#lane' + id).html(data.lane);
                $('#logo' + id).html(data.logo);
                $('#modal-info').modal('hide');//update va2 close modal
                
            }
        })
    });
})
