$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-our-customers",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "our_customers";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var link = $("#link" + id).html();
        $('#update_modal').val(id);
        $('#link_modal').val(link);
     

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var image = $('#image_modal').val();
        var link = $('#link_modal').val();
        var data = {
            'id': id,
            'image': image,
            'link': link
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-our-customers",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#image' + id).html(data.image);
                $('#link' + id).html(data.link);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
