$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-our-team",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "our_team";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var name = $("#name" + id).html();
        var job = $("#job" + id).html();
        var description = $("#description" + id).html();
        $('#update_modal').val(id);
        $('#name_modal').val(name);
        $('#job_modal').val(job);
        $('#description_modal').val(description);
     

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var image = $('#image_modal').val();
        var name = $('#name_modal').val();
        var job = $('#job_modal').val();
        var description = $('#description_modal').val();
        var data = {
            'id': id,
            'image': image,
            'name': name,
            'job': job,
            'description': description,
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-our-team",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#image' + id).html(data.image);
                $('#name' + id).html(data.name);
                $('#job' + id).html(data.job);
                $('#description' + id).html(data.description);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
