$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-our-works",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "our_works";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var menu_title = $("#menu_title" + id).html();
        $('#update_modal').val(id);
        $('#menu_title_modal').val(menu_title);
     

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var menu_title = $('#menu_title_modal').val();
        var data = {
            'id': id,
            'menu_title': menu_title
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-our-works",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#menu_title' + id).html(data.menu);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
