<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurTeam extends Model
{
    protected $table ='ourteam';
    protected $fillable = [
        'image','name', 'job','description'
    ];
    public $timestamps = false;
}
