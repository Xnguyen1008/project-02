<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementMenuOurWorks extends Model
{
    protected $table ='element_menu_ourworks';
    protected $fillable = [
        'image','name_image','link_image','link_evemt','menu_id'
    ];
    public $timestamps = false; 
     
    public function menu()
    {
        return $this->belongsTo('App\our_works','menu_id');
    }
}
