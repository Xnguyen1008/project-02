<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutProcess extends Model
{
    protected $table ='ourprocess';
    protected $fillable = [
        'image','alt_img'
    ];
    public $timestamps = false;
}
