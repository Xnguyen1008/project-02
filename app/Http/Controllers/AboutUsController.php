<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\AboutUs;
class AboutUsController extends Controller
{

    public function showtable()
    {
        $data = AboutUs::all();
        return view('about_us', ['data' => $data]);
    }

    public function insert(Request $request)
    {

        // insert db
        $filename = $_FILES['image']['name'];
        // $filename = $request->image;

        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        $insert = new AboutUs;
        $insert->title ="$request->title";
        $insert->description ="$request->description";
        $insert->image ="$url";
        $insert->alt_img="$request->alt_img";
        $insert->save();
        return redirect()->back();
        
       
    }
    public function delete()
    {
        AboutUs::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $title = $_POST["title"];
        $description = $_POST["description"];
        // $image =$_POST["image"];
        $alt_img=$_POST["alt_img"];
        //update db homepage
        AboutUs::where('id',$ids)->update([
            'title' => $title,
            'description'=>$description,
            // 'image'=>$image,
            'alt_img'=>$alt_img
        ]);
        $data = AboutUs::find($ids);
        return $data;
    }
}




