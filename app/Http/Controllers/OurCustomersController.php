<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\OurCustomers;
class OurCustomersController extends Controller
{
    public function showtable()
    {
        $data = OurCustomers::all();
        return view('ourcustomers', ['data' => $data]);
    }

    public function insert(Request $request)
    {
        // insert db
        $filename = $_FILES['image']['name'];
        // $filename = $request->image;
        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        $insert = new OurCustomers;
        $insert->image ="$url";
        $insert->link="$request->link";
        $insert->save();
        return redirect()->back();
       
    }
    public function delete()
    {
        OurCustomers::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $link=$_POST["link"];
        //update db homepage
        OurCustomers::where('id',$ids)->update([
            'link'=>$link
        ]);
        $data = OurCustomers::find($ids);
        return $data;
    }
}
