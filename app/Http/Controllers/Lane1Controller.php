<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lane1;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class Lane1Controller extends Controller
{
    
    public function showtable()
    {
        $data = Lane1::all();
        return view('about_us.lane1',['data'=>$data]);
    }

   
    public function insert(Request $request)
    {
        
        //image
        if($filename = $_FILES['image']['name']== null){  
            $url =null;
        }else{
        $filename = $_FILES['image']['name'];
        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        }
     
         
        //logo
        $logo = $_FILES['logo']['name'];
        Storage::putFileAs('public',new File($_FILES['logo']['tmp_name']),$logo);
        $url_logo = Storage::url( $logo);

        $insert = new Lane1;
        $insert->title ="$request->title";
        $insert->description ="$request->description";
        $insert->image ="$url";
        $insert->logo ="$url_logo";
        $insert->alt ="$request->alt_img";
        $insert->lane ="$request->lane";
        $insert->save();
        
        return redirect()->back();
    }

  
    public function update()
    {
        // // get value form modal
        $ids = $_POST["id"];
        $title=$_POST["title"];
        $description= $_POST["description"];
        // $image= $_POST["image"];
        $alt= $_POST["alt_img"];
        $lane= $_POST["lane"];
        //update db homepage
        Lane1::where('id',$ids)->update([
            'title'=>$title,
            'description'=>$description,
            'lane'=>$lane,
            'alt'=>$alt,
        ]);
           $data = Lane1::find($ids);
        return  $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        Lane1::find($_POST["id"])->delete();
        return ;
    }
}
