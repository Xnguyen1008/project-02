<?php

namespace App\Http\Controllers;
use App\Homepage;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function showtable()
    {
        $data = Homepage::all();
        return view('home_page', ['data' => $data]);
    }

    public function insert(Request $request)
    {
    
        // insert db
        $insert = new Homepage;
        $insert->first_title ="$request->first_title";
        $insert->last_title ="$request->last_title";
        $insert->save();
        return redirect()->back();
    }
    public function delete()
    {
        Homepage::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $first_title = $_POST["first_title"];
        $last_title = $_POST["last_title"];
        //update db homepage
        Homepage::where('id',$ids)->update([
            'first_title' => $first_title,
            'last_title'=>$last_title
        ]);
        $data = Homepage::find($ids);
        return $data;
    }
}


