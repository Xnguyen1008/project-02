<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\OurTeam;
class OurTeamController extends Controller
{
  
    public function showtable()
    {
        $data = OurTeam::all();
        return view('ourteam', ['data' => $data]);
    }

    public function insert(Request $request)
    {
        // insert db
        $filename = $_FILES['image']['name'];
        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        $insert = new OurTeam;
        $insert->image ="$url";
        $insert->name="$request->name";
        $insert->job="$request->job";
        $insert->description="$request->description";
        $insert->save();
        return redirect()->back();
       
    }
    public function delete()
    {
        OurTeam::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $name=$_POST["name"];
        $job = $_POST["job"];
        $description=$_POST["description"];
        
        //update db homepage
        OurTeam::where('id',$ids)->update([
            'name'=>$name,'job'=>$job,'description'=>$description
        ]);
        $data = OurTeam::find($ids);
        return $data;
    }
}
