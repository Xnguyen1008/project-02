<?php

namespace App\Http\Controllers;
use App\Menu;
use App\HomePage;
use Illuminate\Http\Request;
use App\AboutUs;
use App\OutProcess;
use App\our_works;
use App\OurCustomers;
use App\ElementMenuOurWorks;
use App\OurTeam;
use App\Lane1;
class PageController extends Controller
{
    public function HomePage()
    {
        $data=  Menu::all();
        $dataHomePage=HomePage::all();
        $dataAboutUs =AboutUs::all();
        $dataOurProcess= OutProcess::all();
        $dataOUrWorksMenu= our_works::all();
        $dataOurWorks = ElementMenuOurWorks::all();
        $dataOUrCustomers =OurCustomers::all();
        $dataOurTeam =OurTeam::all();
        return view('page.home',
            [
            'data'=>$data,
            'dataHomePage'=>$dataHomePage,
            'dataAboutUs'=>$dataAboutUs,
            'dataOurProcess'=>$dataOurProcess,
            'dataOUrWorksMenu'=>$dataOUrWorksMenu,
            'dataOurWorks'=>$dataOurWorks,
            'dataOUrCustomers'=>$dataOUrCustomers,
            'dataOurTeam'=>$dataOurTeam,
            ]);
       
    }
    public function AboutUsPage()
    {
        $data1 = Lane1::where('lane',1)->get();
        $data2 = Lane1::select('logo','description','alt')->where('lane',2)->get();
        $data3 = Lane1::where('lane',3)->get();
      return view ('page.about-us',
      [
          'data1'=>$data1,
          'data2'=>$data2,
          'data3'=>$data3,

      ]);
    }
    public function ServicesPage()
    {
        return view('page.services');
    }
    public function ContactMePage()
    {
        return view('page.contact-me');
    }

}
