<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    public function showtable()
    {
        $data = Menu::all();
        return view('menu', ['data' => $data]);
    }

    public function insert(Request $request)
    {
        //get value to form
        $menu_item = $request->menu_item;
        $link_address = $request->link_address;
        $logo = $request->logo;
        // insert db
        $menu = new Menu;
        $menu->menu_item ="$menu_item";
        $menu->link_address ="$link_address";
        $menu->logo ="$logo";
        $menu->save();
        return redirect()->back();
    }
    public function delete()
    {
        Menu::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $menu_item = $_POST["menu_item"];
        $link_address = $_POST["link_address"];
        $logo = $_POST["logo"];
        //update db menu
        Menu::where('id',$ids)->update([
            'menu_item' => $menu_item,
            'link_address'=>$link_address,
            'logo'=>$logo
        ]);

        $data = Menu::find($ids);

        return $data;
    }
}
