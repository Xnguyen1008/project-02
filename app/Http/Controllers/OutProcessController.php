<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OutProcess;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class OutProcessController extends Controller
{
    public function showtable()
    {
        $data = OutProcess::all();
        return view('out_process', ['data' => $data]);
    }

    public function insert(Request $request)
    {

        // insert db
        $filename = $_FILES['image']['name'];
        // $filename = $request->image;

        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        $insert = new OutProcess;
        $insert->image ="$url";
        $insert->alt_img="$request->alt_img";
        $insert->save();
        return redirect()->back();
       
    }
    public function delete()
    {
        OutProcess::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        // $image =$_POST["image"];
        $alt_img=$_POST["alt_img"];
        //update db homepage
        OutProcess::where('id',$ids)->update([
            // 'image'=>$image,
            'alt_img'=>$alt_img
        ]);
        $data = OutProcess::find($ids);
        return $data;
    }
}
