<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
class AuthenticationController extends Controller
{
    public function showRegister()
    {
        return view('admin.register');
    }
    public function Register(RegisterRequest $request)
    {
        $register = new Admin;
        $register ->name ="$request->name";
        $register ->email ="$request->email";
        $register ->username ="$request->username";
        $register ->password =Hash::make($request->password);
        $register ->save();
        return Redirect()->route('showLogin');
    }
    public function showLogin()
    {
        if(!Auth::check())
        {
            return view('admin.login');
        } else {
            return Redirect()->route('admin');
        }
    }
    public function Login(LoginRequest $request)
    {
        $auth = array(
            'username'=>$request->username,
            'password'=>$request->password
        );
        if(Auth::attempt($auth))
        {
            return view('layouts.master_admin');
        }
    }
    public function LogOut()
    {
        Auth::logout();
        return Redirect()->route('showLogin');
    }
    
}
