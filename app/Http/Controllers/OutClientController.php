<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\out_client;
use App\OutProcess;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class OutClientController extends Controller
{
    public function showtable()
    {
        $data_ourprocess=OutProcess::all();
        $data = out_client::all();
        return view('our_client', ['data' => $data,'data_ourprocess' => $data_ourprocess]);
    }

    public function insert(Request $request)
    {
        // insert db
        $filename = $_FILES['image']['name'];
        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        $insert = new out_client;
        $insert->our_id="$request->forign_key";
        $insert->image ="$url";
        $insert->alt_imag="$request->alt_img";
        $insert->save();
        return redirect()->back();
       
    }
    public function delete()
    {
        out_client::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        // $image =$_POST["image"];
        $alt_img=$_POST["alt_img"];
        //update db homepage
        out_client::where('id',$ids)->update([
            'alt_img'=>$alt_img
        ]);
        $data = out_client::find($ids);
        return $data;
    }
}
