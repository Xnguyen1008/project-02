<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|unique:admin,email',
            'username'=>'required|unique:admin,username',
            'password'=>'required',
        ];
    }
    public function messages()
    {
        return[
        'name.required' =>'Vui long nhập họ tên',
        'email.required' =>'Vui long nhập email',
        'email.unique' =>'Email Đã tồn tại',
        'username.required' =>'Vui long nhập username',
        'username.unique' =>'username Đã tồn tại',
        'password.required' =>'Vui long nhập password',
        ];
    }
}
