<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class out_client extends Model
{
    protected $table ='our_client';
    protected $fillable = [
        'image','alt_imag','our_id'
    ];
    public $timestamps = false;
}
