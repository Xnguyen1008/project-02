<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lane1 extends Model
{
    protected $table ='lane1s';
    protected $fillable = [
        'title','image','description','alt'  
    ];
    public $timestamps = false;
}

