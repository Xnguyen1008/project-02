@extends('layouts.master_admin')
@section('table')
   Services Page
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Services  Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>SubTitle</th>
                            <th>Image</th>  
                            <th>Content Subtitle</th>    
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i< count($data); $i++)
                         <tr>
                            <td id="{{$data[$i]->id}}">{{$data[$i]->id}}</td>
                            <td id="title{{$data[$i]->id}}">{{$data[$i]->title}}</td>
                            <td id="subtitle{{$data[$i]->id}}">{{$data[$i]->subtitle}}</td>
                            <td id="image{{$data[$i]->id}}"><img src="{{asset($data[$i]->image)}}" width="20px" height="20px" ></td>
                            <td id="content_subtitle{{$data[$i]->id}}">{{$data[$i]->content_subtitle}}</td>
                            <td width="10px"><button id="modal" type="button" class="btn btn-xs btn-info" value="{{$data[$i]->id}}" data-toggle="modal"
                                    data-target="#modal-info">Edit</button></td>
                            <td width="10px"><button class="btn btn-xs btn-danger btn_delete" type="button" value="{{$data[$i]->id}}">Delete</button></td>
                        </tr>
                            @endfor
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>SubTitle</th>
                            <th>Image</th>  
                            <th>Content Subtitle</th>    
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>

                <h3>Insert:</h3>
                <form class="col-xs-4" method="POST" action="{{route('insert-services')}}" name="insert"  enctype="multipart/form-data" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <hr>
                        <input class="form-control" type="text" name="title" placeholder="Title"><br>
                        <hr>
                        <input class="form-control" type="text" name="subtitle" placeholder="SubTitle"><br>
                        <hr>
                        <input type="file" id="file_img" name="image">
                        <hr>
                        <input class="form-control" type="text" name="content_subtitle" placeholder="Content Subtitle"><br>
                        <hr>
                        <input class="btn btn-lg btn-primary" type="submit" value="Insert">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<div class="modal modal-info fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Title</label>
                    <input class="form-control" type="text" id="title_modal" placeholder=""><br>
                    <hr>
                    <label>SubTitle</label>
                    <input class="form-control" type="text" id="subtitle_modal" placeholder=""><br>
                    <hr>
                    <label>Image</label>
                    <input type="file" id="file_img_modal" name="image_modal">
                    <hr>
                    <label>Content Subtitle</label><br>
                    <input class="form-control" type="text" id="content_subtitle_modal" placeholder=""><br>
                    <hr>
                   
                </div>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline btn_update" id="update_modal">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script src="{{asset('./js/services/services.js')}}"></script>
<!-- /.row -->
@endsection