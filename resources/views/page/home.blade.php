﻿<!DOCTYPE html>
<!-- saved from url=(0101)https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0 -->
<html class="js" lang="vi" itemscope="" itemtype="https://schema.org/WebPage">
<!-- head -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/gmv-logo.png')}}">
    <link rel="apple-touch-icon" href="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/gmv-logo.png')}}">

    <!-- wp_head() -->
    <title>GMV DESIGN – GMV DESIGN</title>
    <!-- script | dynamic -->
    <script id="mfn-dnmc-config-js">
        //<![CDATA[
        window.mfn_ajax ="https://gmvdesign.gmvagency.com/wp-admin/admin-ajax.php";
        window.mfn = { mobile_init: 1240, nicescroll: 40, parallax:"translate3d", responsive: 1, retina_js: 0 };
        window.mfn_lightbox = { disable: false, disableMobile: false, title: false, };
        window.mfn_sliders = { blog: 0, clients: 0, offer: 0, portfolio: 0, shop: 0, slider: 0, testimonials: 0 };
        //]]>
    </script>
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin GMV DESIGN »" href="https://gmvdesign.gmvagency.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi GMV DESIGN »" href="https://gmvdesign.gmvagency.com/comments/feed/">
    <link rel="alternate" type="application/rss+xml" title="GMV DESIGN » HOME Dòng phản hồi" href="https://gmvdesign.gmvagency.com/home/feed/">

    <link rel="stylesheet" id="layerslider-css" href="{{asset('./statics/css/layerslider.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="{{asset('./statics/css/styles.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="{{asset('./statics/css/settings.css')}}" type="text/css" media="all">


    <link rel="stylesheet" id="mfn-base-css" href="{{asset('./statics/css/base.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-layout-css" href="{{asset('./statics/css/layout.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-shortcodes-css" href="{{asset('./statics/css/shortcodes.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-animations-css" href="{{asset('./statics/css/animations.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-jquery-ui-css" href="{{asset('./statics/css/jquery.ui.all.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-jplayer-css" href="{{asset('./statics/css/jplayer.blue.monday.css')}}" type="text/css" media="all">

    <link rel="stylesheet" id="style-css" href="{{asset('./statics/css/style.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-responsive-css" href="{{asset('./statics/css/responsive.css')}}" type="text/css" media="all">

    <link rel="stylesheet" id="Roboto-css" href="{{asset('./statics/css/css.css')}}" type="text/css" media="all">

    <script type="text/javascript" src="{{asset('./statics/js/jquery.js')}}"></script>

    <!-- script | retina -->
    <script id="mfn-dnmc-retina-js">
        //<![CDATA[
        jQuery(window).load(function () {
            var retina = window.devicePixelRatio > 1 ? true : false;
            if (retina) {
                var retinaEl = jQuery("#logo img.logo-main");
                var retinaLogoW = retinaEl.width();
                var retinaLogoH = retinaEl.height();
                retinaEl.attr("src",
                    "https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/logo_gmvdesign.png')}}").width(
                    retinaLogoW).height(retinaLogoH);
                var stickyEl = jQuery("#logo img.logo-sticky");
                var stickyLogoW = stickyEl.width();
                var stickyLogoH = stickyEl.height();
                stickyEl.attr("src",
                    "https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/logo_gmvdesign.png')}}").width(
                    stickyLogoW).height(stickyLogoH);
                var mobileEl = jQuery("#logo img.logo-mobile");
                var mobileLogoW = mobileEl.width();
                var mobileLogoH = mobileEl.height();
                mobileEl.attr("src",
                    "https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/logo_gmvdesign.png')}}").width(
                    mobileLogoW).height(mobileLogoH);
                var mobileStickyEl = jQuery("#logo img.logo-mobile-sticky");
                var mobileStickyLogoW = mobileStickyEl.width();
                var mobileStickyLogoH = mobileStickyEl.height();
                mobileStickyEl.attr("src",
                    "https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/logo_gmvdesign.png')}}").width(
                    mobileStickyLogoW).height(mobileStickyLogoH);
            }
        });
        //]]>
    </script>

    <script type="text/javascript">
        function setREVStartSize(e) {
            try {
                var i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] ||
                    e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" ==
                    e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                                u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                            }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset
                            .length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 !=
                            e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({
                    height: f
                })
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };
    </script>
</head>

<!-- body -->

<body class="home page-template-default page page-id-2254 template-slider  color-custom style-default button-stroke layout-full-width if-zoom if-border-hide no-content-padding header-below minimalist-header-no sticky-header sticky-tb-color ab-hide subheader-both-center menu-line-below-80-1 menuo-no-borders logo-no-margin logo-no-sticky-padding footer-copy-center mobile-tb-left mobile-mini-mr-ll be-1783 wpb-js-composer js-comp-ver-5.1.1 vc_responsive nice-scroll">

    <!-- mfn_hook_top -->
    <!-- mfn_hook_top -->
    <!-- #Wrapper -->
    <div id="Wrapper">
        <!-- #Header_bg -->
        <div id="Header_wrapper">

            <!-- #Header -->
            <header id="Header">
                <!-- .header_placeholder 4sticky  -->
                <div class="header_placeholder" style="height: 70px;"></div>

                <div id="Top_bar" class="is-sticky" style="top: 0px;">

                    <div class="container">
                        <div class="column one">

                            <div class="top_bar_left clearfix" style="width: 1080px;">

                                <!-- Logo -->
                                <div class="logo"><a id="logo" href="https://gmvdesign.gmvagency.com/" title="GMV DESIGN"><img
                                            class="logo-main scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"><img class="logo-sticky scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"><img class="logo-mobile scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"><img class="logo-mobile-sticky scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"></a></div>
                                <div class="menu_wrapper">
                                    <nav id="menu" class="menu-main-menu-left-container">
                                        <ul id="menu-main-menu-left" class="menu">
                                            @for ($i = 0; $i < count($data); $i++)
                                            <li id="menu-item-3117" class="menu-item menu-item-type-post_type menu-item-object-page last"><a
                                            href="{{url($data[$i]->link_address)}}"><span>{{$data[$i]->menu_item}}</span></a></li>
                                            @endfor
                                        </ul>
                                    </nav><a class="responsive-menu-toggle" href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#"><i
                                            class="icon-menu-fine"></i></a>
                                </div>

                                <div class="secondary_menu_wrapper">
                                    <!-- #secondary-menu -->
                                    <nav id="secondary-menu" class="menu-main-menu-right-container">
                                        <ul id="menu-main-menu-right" class="secondary-menu">
                                            <li id="menu-item-2412" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2412"><a
                                                    href="https://gmvdesign.gmvagency.com/about-2/">About</a></li>
                                            <li id="menu-item-2411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2411"><a
                                                    href="https://gmvdesign.gmvagency.com/contact-me/">Contact me</a></li>
                                            <li id="menu-item-2391" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2391"><a
                                                    target="_blank" href="http://themeforest.net/item/betheme-responsive-multipurpose-wordpress-theme/7758048?ref=muffingroup">Buy
                                                    now</a></li>
                                        </ul>
                                    </nav>
                                </div>

                                <div class="banner_wrapper">
                                </div>

                                <div class="search_wrapper">
                                    <!-- #searchform -->
                                    <form method="get" id="searchform" action="https://gmvdesign.gmvagency.com/">
                                        <i class="icon_search icon-search-fine"></i>
                                        <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#"
                                            class="icon_close"><i class="icon-cancel-fine"></i></a>
                                        <input type="text" class="field" name="s" id="s" placeholder="Enter your search">
                                        <input type="submit" class="submit" value="" style="display:none;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>

        <!-- mfn_hook_content_before -->
        <!-- mfn_hook_content_before -->
        <!-- #Content -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <!-- .sections_group -->
                <div class="sections_group">
                    <div class="entry-content" itemprop="mainContentOfPage">
                        <div class="section mcb-section" style="padding-top:80px; padding-bottom:0px; background-color:">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix"></div>
                                        </div>
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style=" padding:20px;">
                                                <div class="title-heading">
                                                    <h2>{{$dataHomePage[0]->first_title}}<span class="color-title"> {{$dataHomePage[0]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one-second column_column  column-margin-">
                                            <div class="column_attr clearfix">
                                                <p>
                                                    <b><br>{{$dataAboutUs[0]->title}}</b>
                                                    <br>{{$dataAboutUs[0]->description}}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one-second column_image">
                                            <div class="image_frame image_item no_link scale-with-grid aligncenter stretch no_border">
                                                <div class="image_wrapper"><img class="scale-with-grid" src="{{asset($dataAboutUs[0]->image)}}"
                                                        alt="about-gmvdesign" width="608" height="405"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section mcb-section no-margin-h no-margin-v  hide-mobile" style="padding-top:60px; padding-bottom:60px; background-color:"
                            data-parallax="3d">
                            <img class="mfn-parallax" src="{{asset('./statics/images/skills.jpg')}}" alt="" style="width: 1320px; height: 879.656px; transform: translate3d(0px, -169.828px, 0px);">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div class="title-heading">
                                                    <h2>{{$dataHomePage[1]->first_title}}<span class="color-title"> {{$dataHomePage[1]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one column_image">
                                            <div class="image_frame image_item no_link scale-with-grid aligncenter stretch no_border">
                                                <div class="image_wrapper"><img class="scale-with-grid" src="{{asset($dataOurProcess[0]->image)}}"
                                                        alt="{{$dataOurProcess[0]->alt_img}}" width="1676" height="381"></div>
                                            </div>
                                        </div>  
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div class="section mcb-section no-margin-h no-margin-v  hide-desktop hide-tablet" style="padding-top: 60px; padding-bottom: 60px;"
                            data-parallax="3d">
                            <img class="mfn-parallax" src="{{asset('./statics/images/skills.jpg')}}" alt="" style="width: 810.317px; height: 540px; transform: translate3d(-405.158px, 0px, 0px);">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div class="title-heading">
                                                    <h2>{{$dataHomePage[1]->first_title}}<span class="color-title"> {{$dataHomePage[1]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one column_image">
                                            <div class="image_frame image_item no_link scale-with-grid aligncenter stretch no_border">
                                                <div class="image_wrapper"><img class="scale-with-grid" src="{{asset('./statics/images/process-mobile.png')}}"
                                                        alt="process-mobile" width="373" height="1506"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section mcb-section no-margin-h no-margin-v" style="padding-top:80px; padding-bottom:40px; background-color:#2b2c30">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div class="title-heading">
                                                    <h2>{{$dataHomePage[2]->first_title}}<span class="color-title"> {{$dataHomePage[2]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_portfolio">
                                            <div class="column_filters">
                                                <div id="Filters" class="isotope-filters filters4portfolio" data-parent="column_filters">
                                                    <div class="filters_wrapper">
                                                        <ul class="categories">
                                                            <li class="reset current-cat">
                                                                <a class="all" data-rel="*" href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">All</a>
                                                            </li>
                                                           @foreach ($dataOUrWorksMenu as $item)
                                                            <li class='{{strtolower("$item->menu")}}'>
                                                                    <a data-rel='.category-{{strtolower("$item->menu")}}' href="#">{{$item->menu}}</a>
                                                            </li>
                                                           @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="portfolio_wrapper isotope_wrapper">
                                                    <ul class="portfolio_group lm_wrapper isotope col-4 masonry-hover"
                                                        style="position: relative; height: 668.016px;">
                                                        @foreach ($dataOurWorks as $item)
                                                            <li class="portfolio-item isotope-item category-{{strtolower($item->menu->menu)}}  has-thumbnail"
                                                                style="position: absolute; left: 0px; top: 0px;">
                                                                <div class="masonry-hover-wrapper">
                                                                    <div class="hover-desc bg-dark" style="">
                                                                        <div class="desc-inner">
                                                                            <h3 class="entry-title" itemprop="headline">
                                                                                <a class="link" href="{{url($item->link_event)}}">{{$item->name_image}}</a>
                                                                            </h3>
                                                                            <div class="desc-wrappper"></div>
                                                                        </div>
                                                                        <div class="links-wrappper clearfix">
                                                                            <a class="zoom" href="{{asset($item->image)}}"
                                                                                rel="lightbox" data-type="image">
                                                                                <i class="icon-search"></i>
                                                                            </a>
                                                                            <a class="link" href="{{url($item->link_event)}}">
                                                                                <i class="icon-link"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="image-wrapper scale-with-grid">
                                                                        <a class="link" href="{{url($item->link_event)}}">
                                                                            <img width="650" height="520" src="{{asset($item->image)}}"
                                                                                class="scale-with-grid wp-post-image" alt=""
                                                                                itemprop="image" srcset="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/11/123-1.jpg 650w, https://gmvdesign.gmvagency.com/wp-content/uploads/2017/11/123-1-300x240.jpg 300w, https://gmvdesign.gmvagency.com/wp-content/uploads/2017/11/123-1-183x146.jpg 183w, https://gmvdesign.gmvagency.com/wp-content/uploads/2017/11/123-1-50x40.jpg 50w, https://gmvdesign.gmvagency.com/wp-content/uploads/2017/11/123-1-94x75.jpg 94w"
                                                                                sizes="(max-width: 650px) 100vw, 650px">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    <div class="column one pager_wrapper pager_lm">
                                                        <a class="pager_load_more button button_js kill_the_icon" href="https://gmvdesign.gmvagency.com/page/2/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0">
                                                            <span class="button_icon">
                                                                <i class="icon-layout"></i>
                                                            </span>
                                                            <span class="button_label">Load more</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section mcb-section" style="padding-top:80px; padding-bottom:40px; background-color:">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div class="title-heading">
                                                        <h2>{{$dataHomePage[3]->first_title}}<span class="color-title"> {{$dataHomePage[3]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_clients">
                                            <ul class="clients clearfix">
                                               @for ($i = 0; $i < count($dataOUrCustomers); $i++)
                                                    <li style="width:20%">
                                                        <div class="client_wrapper">
                                                            <a target="_blank" href="{{$dataOUrCustomers[$i]->link}}"
                                                                title="Client {{$i+1}}">
                                                                <div class="gs-wrapper">
                                                                    <img width="75" height="75" src="{{asset($dataOUrCustomers[$i]->image)}}"
                                                                        class="scale-with-grid wp-post-image" alt="" 
                                                                        sizes="(max-width: 75px) 100vw, 75px">
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </li>
                                                @endfor
                                              
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section mcb-section" style="padding-top:0px; padding-bottom:0px; background-color:"
                            data-parallax="3d">
                            <img class="mfn-parallax" src="{{asset('./statics/images/parallax-02.jpg')}}" alt="" style="width: 1320px; height: 825px; transform: translate3d(0px, 259px, 0px);">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_divider">
                                            <hr class="no_line" style="margin: 0 auto 600px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section mcb-section" style="padding-top:80px; padding-bottom:60px; background-color:">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div class="title-heading">
                                                    <h2>{{$dataHomePage[4]->first_title}}<span class="color-title"> {{$dataHomePage[4]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @for ($i = 0; $i < count($dataOurTeam); $i++)
                                    @if ($i==0)
                                        <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                            <div class="mcb-wrap-inner">
                                                <div class="column mcb-column one-second column_image">
                                                    <div class="image_frame image_item no_link scale-with-grid alignright no_border">
                                                        <div class="image_wrapper"><img class="scale-with-grid" src="{{asset($dataOurTeam[$i]->image)}}"
                                                                alt="Tien Cherry" width="295" height="406"></div>
                                                    </div>
                                                </div>
                                                <div class="column mcb-column one-second column_column  column-margin-">
                                                    <div class="column_attr clearfix align_left" style=" padding:80px 0px 0px 0px;">
                                                        <span style="font-weight:bold; font-size:18px;">{{$dataOurTeam[$i]->name}}</span><br>
                                                        <font style="font-weight:100;">
                                                                {{$dataOurTeam[$i]->job}}
                                                        </font>
                                                        <div class="border-title-lv"></div>

                                                        <p style="font-weight:100;">
                                                                {{$dataOurTeam[$i]->description}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="wrap mcb-wrap one-fourth  valign-top clearfix" style="">
                                            <div class="mcb-wrap-inner">
                                                <div class="column mcb-column five-sixth column_hover_box team-avatar">
                                                    <div class="hover_box">
                                                        <div class="hover_box_wrapper"><img class="visible_photo scale-with-grid"
                                                                src="{{asset($dataOurTeam[$i]->image)}}"
                                                                alt="" width="1168" height="1168"><img
                                                                class="hidden_photo scale-with-grid" src="{{asset($dataOurTeam[$i]->image)}}"
                                                                alt="" width="1168" height="1168"></div>
                                                    </div>
                                                </div>
                                                <div class="column mcb-column five-sixth column_column  column-margin-">
                                                    <div class="column_attr clearfix align_center" style=" padding:20px 0px;; border: 1px solid #4e595f;">
                                                        <p style="font-size:20px;">{{$dataOurTeam[$i]->name}}</p>
                                                        <p style="font-size:16px;">{{$dataOurTeam[$i]->job}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                               @endfor
                            </div>
                        </div>
                        <div class="section mcb-section" style="padding-top:80px; padding-bottom:60px; background-color:#2b2c30">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div class="title-heading">
                                                        <h2>{{$dataHomePage[5]->first_title}}<span class="color-title"> {{$dataHomePage[5]->last_title}}</span></h2>
                                                    <div class="border-title"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one-second column_column  column-margin-">
                                            <div class="column_attr clearfix" style="">
                                                <h3>Contact Details</h3>
                                                <p style="font-weight:100;">Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis
                                                    ex explicabo vitae nostrum facilis asperiores dolorem illo officiis
                                                    ratione vel fugiat dicta laboriosam labore adipisci.</p>
                                            </div>
                                        </div>
                                        <div class="column mcb-column one-second column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <div role="form" class="wpcf7" id="wpcf7-f2446-p2254-o1" lang="en-US"
                                                    dir="ltr">
                                                    <div class="screen-reader-response"></div>
                                                    <form action="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#wpcf7-f2446-p2254-o1"
                                                        method="post" class="wpcf7-form" novalidate="novalidate">
                                                        <div style="display: none;">
                                                            <input type="hidden" name="_wpcf7" value="2446">
                                                            <input type="hidden" name="_wpcf7_version" value="4.9.1">
                                                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2446-p2254-o1">
                                                            <input type="hidden" name="_wpcf7_container_post" value="2254">
                                                        </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap your-subject"><input
                                                                    type="text" name="your-subject" value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                    aria-required="true" aria-invalid="false"
                                                                    placeholder="Title"></span> </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap your-name"><input
                                                                    type="text" name="your-name" value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                    aria-required="true" aria-invalid="false"
                                                                    placeholder="Full Name"></span> </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap your-email"><input
                                                                    type="email" name="your-email" value="" size="40"
                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                    aria-required="true" aria-invalid="false"
                                                                    placeholder="Email"></span> </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap tel-684"><input
                                                                    type="tel" name="tel-684" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                    aria-required="true" aria-invalid="false"
                                                                    placeholder="Phone"></span></div>
                                                        <div class="column one"><span class="wpcf7-form-control-wrap your-message"><textarea
                                                                    name="your-message" cols="40" rows="8" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                                                    aria-required="true" aria-invalid="false"
                                                                    placeholder="Message to GMV Design..."></textarea></span></div>
                                                        <div class="column one"><input type="submit" value="SEND" class="wpcf7-form-control wpcf7-submit button_full_width"><span
                                                                class="ajax-loader"></span></div>
                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section section-page-footer">
                            <div class="section_wrapper clearfix">

                                <div class="column one page-pager">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- mfn_hook_content_after -->
        <!-- mfn_hook_content_after -->
        <!-- #Footer -->
        <footer id="Footer" class="clearfix">


            <div class="widgets_wrapper" style="padding:50px 0 35px;">
                <div class="container">
                    <div class="column one-third">
                        <aside id="text-3" class="widget widget_text">
                            <h4>GMV DESIGN</h4>
                            <div class="textwidget">
                                <ul style="line-height: 32px;">
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Tax
                                            : 0 3 1 4 5 0 4 1 4 2</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Cell:
                                            0163 977 7548</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Email
                                            : cherrytien@gmvgroup.vn</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Address
                                            : 62 Nguyen Quang Bich str , ward 13 , Tan Binh District , HCMC , Vietnam.
                                        </a></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="column one-third">
                        <aside id="text-4" class="widget widget_text">
                            <h4>SERVICES</h4>
                            <div class="textwidget">
                                <ul style="line-height: 32px;">
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Branding
                                            Design</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Creative
                                            Design</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Digital
                                            Design</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">Storyboard
                                            Clip </a></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="column one-third">
                        <aside id="text-5" class="widget widget_text">
                            <h4>POLICY</h4>
                            <div class="textwidget">
                                <ul style="line-height: 32px;">
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">GMV
                                            Client </a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">GMV
                                            Partnership</a></li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/?fbclid=IwAR2Fu-fHT3Mfsp1SBKNUzS2DuNOmLOT3G5jr4vdqV76AREbO7u5XA3OW6a0#">GMV
                                            Business Group</a></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>


            <div class="footer_copy">
                <div class="container">
                    <div class="column one">


                        <!-- Copyrights -->
                        <div class="copyright">
                            © 2018 GMV DESIGN. All Rights Reserved. <a target="_blank" rel="nofollow" href="http://muffingroup.com/">Muffin
                                group</a>
                        </div>

                        <ul class="social"></ul>
                    </div>
                </div>
            </div>





        </footer>

    </div><!-- #Wrapper -->
    <script type="text/javascript" src="{{asset('./statics/js/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/core.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/widget.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/mouse.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/sortable.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/tabs.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/accordion.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/menu.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/animations.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jplayer.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/translate3d.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/scripts(1).js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/comment-reply.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/wp-embed.min.js')}}"></script>
</body>

</html>