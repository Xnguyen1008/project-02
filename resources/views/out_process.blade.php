@extends('layouts.master_admin')
@section('table')
    Home Page   
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Out Process Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>image</th>
                            <th>Alt Img</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i< count($data); $i++)
                            <tr>
                                <td id="{{$data[$i]->id}}">{{$data[$i]->id}}</td>
                                <td id="image{{$data[$i]->id}}"><img src="{{($data[$i]->image) }}" width="60px" height="30px" ></td>
                                <td id="alt_img{{$data[$i]->id}}">{{$data[$i]->alt_img}}</td>
                                <td width="10px"><button id="modal" type="button" class="btn btn-xs btn-info" value="{{$data[$i]->id}}" data-toggle="modal" data-target="#modal-info"><i class="fa fa-fw fa-pencil"></i>Edit</button></td>
                                <td width="10px"><button class="btn btn-xs btn-danger btn_delete" type="button" value="{{$data[$i]->id}}"><i class="fa fa-fw fa-trash-o"></i>Delete</button></td>
                            </tr>   
                        @endfor
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>image</th>
                            <th>Alt Img</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>

                <h3>Insert:</h3>
                <form class="col-xs-4" method="POST" action="{{route('insert_out-process')}}" name="insert"  enctype="multipart/form-data" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="file" id="file_img" name="image">
                        <hr>
                        <input class="form-control" type="text" name="alt_img" placeholder="Alt Img"><br>
                        <hr>
                        <input class="btn btn-lg btn-primary" type="submit" value="Insert">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<div class="modal modal-info fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Image</label>
                    <input type="file" id="file_img_modal" name="image_modal">
                    <label>Alt Img</label>
                    <input class="form-control" type="text" id="alt_img_modal" placeholder=""><br>
                    <hr>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline btn_update" id="update_modal">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script src="{{asset('./js/homepage/out_process.js')}}"></script>
<!-- /.row -->
@endsection