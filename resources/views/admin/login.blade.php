@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
        <!-- Horizontal Form -->
        <div class="card">
          <div class="card-header">
            <h3 class="box-title"> Login</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="card-body">
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                    <strong>Lỗi</strong>
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                          @endforeach
                      </ul>
                </div>
                @endif
        <form class="form-horizontal" method="POST" action="{{route('Login')}}">
          @csrf
              <div class="form-group">
                <label  class="col-sm-2 control-label">UserName</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control"  name="username" placeholder="User Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      {{-- <input type="checkbox"> Remember me <br>
                      <button type="submit" class="btn btn-default">Cancel</button> --}}
              <button type="submit" class="btn btn-info pull-right">Sign in</button>
                    </label>
                  </div>
                </div>
              </div>
           
          </form>
        </div>
        </div>
    </div>
  </div>
</div>  
@endsection