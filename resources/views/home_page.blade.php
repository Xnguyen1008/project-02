@extends('layouts.master_admin')
@section('table')
    Home table
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Title</th>
                            <th>Last Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i< count($data); $i++)
                            <tr>
                                <td id="{{$data[$i]->id}}">{{$data[$i]->id}}</td>
                                <td id="first_title{{$data[$i]->id}}">{{$data[$i]->first_title}}</td>
                                <td id="last_title{{$data[$i]->id}}">{{$data[$i]->last_title}}</td>
                                <td width="10px"><button id="modal" type="button" class="btn btn-xs btn-info" value="{{$data[$i]->id}}" data-toggle="modal"
                                        data-target="#modal-info">Edit</button></td>
                                <td width="10px"><button class="btn btn-xs btn-danger btn_delete" type="button" value="{{$data[$i]->id}}">Delete</button></td>
                            </tr>
                        @endfor
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>First Title</th>
                            <th>Last Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>

                <h3>Insert:</h3>
                <form class="col-xs-4" method="POST" action="{{route('insert_homepage')}}" name="insert" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <hr>
                        <input class="form-control" type="text" name="first_title" placeholder="First Title"><br>
                        <hr>
                        <input class="form-control" type="text" name="last_title" placeholder="Last Title"><br>
                        <hr>
                        <input class="btn btn-lg btn-primary" type="submit" value="Insert">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<div class="modal modal-info fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>First title</label>
                    <input class="form-control" type="text" id="first_title_modal" placeholder=""><br>
                    <hr>
                    <label>Last Title</label>
                    <input class="form-control" type="text" id="last_title_modal" placeholder=""><br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline btn_update" id="update_modal">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /.row -->
<script src="{{asset('./js/homepage/homepage.js')}}"></script>
@endsection