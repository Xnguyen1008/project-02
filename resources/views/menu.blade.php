@extends('layouts.master_admin')
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Menu Item</th>
                            <th>Link Address</th>
                            <th>Logo</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i< count($data); $i++)
                        <tr>
                            <td id="{{$data[$i]->id}}">{{$data[$i]->id}}</td>
                            <td id="menu_item{{$data[$i]->id}}">{{$data[$i]->menu_item}}</td>
                            <td id="link_address{{$data[$i]->id}}">{{$data[$i]->link_address}}</td>
                            <td id="logo{{$data[$i]->id}}">{{$data[$i]->logo}}</td>
                            <td width="10px"><button id="modal" type="button" class="btn btn-xs btn-info" value="{{$data[$i]->id}}" data-toggle="modal" data-target="#modal-info"><i class="fa fa-fw fa-pencil"></i>Edit</button></td>
                            <td width="10px"><button class="btn btn-xs btn-danger btn_delete" type="button" value="{{$data[$i]->id}}"><i class="fa fa-fw fa-trash-o"></i>Delete</button></td>
                        </tr>
                            @endfor
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Menu Item</th>
                            <th>Link Address</th>
                            <th>Logo</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>

                <h3>Insert:</h3>
                <form class="col-xs-4" method="POST" action="{{route('insert_menu')}}" name="insert" role="form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <hr>
                        <input class="form-control" type="text" name="menu_item" placeholder="Menu item"><br>
                        <hr>
                        <input class="form-control" type="text" name="link_address" placeholder="Link Address"><br>
                        <hr>
                        <input class="form-control" type="text" name="logo" placeholder="Logo">
                        <hr>
                        <input class="btn btn-lg btn-primary" type="submit" value="Insert">
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<div class="modal modal-info fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Menu Item</label>
                    <input class="form-control" type="text" id="menu_item_modal" placeholder=""><br>
                    <hr>
                    <label>Link Address</label>
                    <input class="form-control" type="text" id="link_address_modal" placeholder=""><br>
                    <hr>
                    <label>Logo</label>
                    <input class="form-control" type="text" id="logo_modal" placeholder="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline btn_update" id="update_modal">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /.row -->
@endsection




<script src="{{asset('./js/menu/menu.js')}}"></script>